﻿using BlumbitMongoApi.DBContext;
using BlumbitMongoApi.Models;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlumbitMongoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly MongoDBContext _context;

        // Constructor
        public ProductsController(MongoDBContext context)
        {
            _context = context;
        }

        // GET: api/<ProductsController>
        // Async
        [HttpGet]
        public async Task<IEnumerable<Product>> Get()
        {
            return await _context.Products.Find(_ => true).ToListAsync();
        }

        // GET api/<ProductsController>/GUID
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> Get(string id)
        {
            var singleProduct = await _context.Products.Find(sp => sp.Id == id).FirstOrDefaultAsync();
            if (singleProduct == null) { return NotFound(); }

            return singleProduct;
        }

        // POST api/<ProductsController>
        [HttpPost]
        public async Task<ActionResult<Product>> Create([FromBody] Product newProduct)
        {
            await _context.Products.InsertOneAsync(newProduct);
            return CreatedAtRoute(new { id = newProduct.Id }, newProduct);
        }

        // PUT api/<ProductsController>/GUID
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(string id, [FromBody] Product updateProduct)
        {
            var updatedProduct = await _context.Products.Find(sp => sp.Id == id).FirstOrDefaultAsync();
            if (updatedProduct == null) { return NotFound(); }

            await _context.Products.ReplaceOneAsync(up => up.Id == id, updateProduct);

            return Accepted();
        }

        // DELETE api/<ProductsController>/GUID
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var deletedProduct = await _context.Products.Find(sp => sp.Id == id).FirstOrDefaultAsync();
            if (deletedProduct == null) { return NotFound(); }

            await _context.Products.DeleteOneAsync(dp => dp.Id == id);

            return NoContent();
        }
    }
}
