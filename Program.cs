using BlumbitMongoApi.DBContext;
using BlumbitMongoApi.Models;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Connect to MongoDB
builder.Services.Configure<MongoDBSettings>(builder.Configuration.GetSection(nameof(MongoDBSettings)));

// Using the Singleton Pattern
builder.Services.AddSingleton(serviceProvider =>
    {
        var settings = serviceProvider.GetRequiredService<IOptions<MongoDBSettings>>().Value;
        return new MongoDBContext(settings.ConnectionString, settings.DatabaseName);
    });

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
