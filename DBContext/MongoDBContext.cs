﻿using BlumbitMongoApi.Models;
using MongoDB.Driver;

namespace BlumbitMongoApi.DBContext
{
    public class MongoDBContext
    {
        private readonly IMongoDatabase _database;

        // Constructor
        public MongoDBContext(string connectionString, string databaseName)
        {
            var client = new MongoClient(connectionString);
            _database = client.GetDatabase(databaseName);
        }

        // En lugar de realizar la Migracion, lo que se hace aqui es comunicarse
        // con la colleción en la BBDD de MongoDB
        public IMongoCollection<Product> Products => _database.GetCollection<Product>("Products");
    }
}
